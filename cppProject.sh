#!/bin/bash
# file: cppProject.sh

mkdir $1 $1/src $1/lib $1/doc $1/build-debug $1/build-release
touch $1/README.MD $1/CMakeLists.txt $1/src/$1.cpp

echo "# $1" >> $1/README.MD
echo "project($1)" >> $1/CMakeLists.txt
echo "// $1" >> $1/src/$1.cpp

cd $1
git init
git add README.MD CMakeLists.txt src
git commit -m "First commit"

bitbucket create_from_local --private

cd ..