#!/bin/bash
# file: pyProject.sh

mkdir $1 $1/$1 $1/lib $1/doc
touch $1/README.MD $1/$1/__init__.py

echo "# $1" >> $1/README.MD
echo "// $1" >> $1/$1/__init__.py

cd $1
git init
git add README.MD $1
git commit -m "First commit"

bitbucket create_from_local --private

cd ..