#!/bin/bash
# file: runNuke10.5.sh

nukeVersion=$1

# Check for empty nuke version parameter
if [ -z "$nukeVersion" ]; then
	echo "A version of Nuke must be specified";
	exit;
fi

LD_PRELOAD=/usr/lib64/libstdc++.so.6:/lib64/libgcc_s.so.1

# Check for the right nuke version execution
if [[ "$nukeVersion" = "10" ]]; then

	echo "Running NUKE 10.5"

	/usr/local/Nuke10.5v5/Nuke10.5 --nc

elif [[ "$nukeVersion" = "11" ]]; then

	echo "Running NUKE 11.0"

	/usr/local/Nuke11.0v1/Nuke11.0 --nc

else
	echo "A right version of Nuke must be specified";
fi
